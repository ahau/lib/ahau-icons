# ahau-icons

## Example Usage

```js
const path = require('path')
const buildIcons = require('ahau-icons')

buildIcons({
  icon: path.join(__dirname, 'icon.png'),
  setupImage: path.join(__dirname, 'setup-icon.svg')
})

// outputs icon files into:
//   - __dirname/win
//   - __dirname/mac
```
