const fs = require('fs')
const path = require('path')
const sharp = require('sharp')
const toIco = require('to-ico')
const { convert: icnsConvert } = require('@fiahfy/icns-convert')

const SIZES = [16, 24, 32, 48, 64, 96, 128, 256]

module.exports = async function buildIcons (input) {
  const icon = sharp(input.icon)
  const setupImage = sharp(input.setupImage)

  const dirname = path.dirname(input.icon)
  // assumes that icon + setupImage are in the same dir

  // windows icons
  buildIcon(icon, SIZES, createIco, dirname, 'icon')
  buildIcon(setupImage, SIZES, createIco, dirname, 'setup-icon')

  // mac icons
  buildIcon(icon, [...SIZES, 512, 1024], createIcns, dirname, 'icon')
  buildIcon(setupImage, [...SIZES, 512, 1024], createIcns, dirname, 'dmg-icon')
}


async function buildIcon (image, sizes, createIcon, dirname, fileName) {
  await Promise
    .all(sizes.map(size => makeIcon(size, image)))
    .then(bufs => {
      createIcon(bufs, dirname, fileName)
      console.log(`- ${fileName}`)
    })
    .catch(err => console.error('Error building icons', err))
}

function makeIcon (size, image) {
  return image
    .resize(size, size)
    .png()
    .toBuffer()
}

function createIco (bufs, dirname, fileName) {
  toIco(bufs)
    .then(buf => {
      fs.writeFileSync(path.join(dirname, `win/${fileName}.ico`), buf)
    })
    .catch(err => console.error('createIco error:', err))
}

function createIcns (bufs, dirname, fileName) {
  icnsConvert(bufs)
    .then(data => {
      fs.writeFileSync(path.join(dirname, `mac/${fileName}.icns`), data)
    })
    .catch(err => console.error('createIcns error: ', err))
}
